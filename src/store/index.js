import { createStore } from 'vuex'

export default createStore({
  state: {
    paises: [],
    paisesFiltrados: []
  },
  mutations: {   //modificar el state
    setPaises(state, payload){
      state.paises = payload
    },
    setPaisesFiltrados(state, payload){
      state.paisesFiltrados = payload
    }
  },
  actions: {
    async getPaises({commit}){
      try {
        const res = await fetch('https://restcountries.eu/rest/v2/all');
        const data = await res.json();
        commit('setPaises', data);
      } catch (error) {
        console.log(error);
      }
    },
    filtrarRegion({commit,state}, region){
        const data = state.paises.filter(pais => {
          return region !== '' 
          ? pais.region == region
          : pais.region != region
        });
        commit('setPaisesFiltrados', data);
    },
    filtroNombre({commit,state}, texto){
      const textoCliente = texto.toLowerCase();
      const data = state.paises.filter(pais => {
        const textoApi = pais.name.toLowerCase();
        if(textoApi.includes(textoCliente)){
           return pais;
        }
      });
      commit('setPaisesFiltrados', data);
    }
  },
  getters:{ //simpre retornan algo
        topPaisesPoblacion(state){
          return state.paisesFiltrados.sort((a,b)=> a.population < b.population ? 1 : -1)
        }
  },
  modules: {
  }
})
